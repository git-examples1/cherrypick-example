---- SQL Queries ------

drop table employee;
drop table employees;

create table employees (
employee_id     int             not null,
first_name      varchar(20)     not null,
last_name       varchar(20)     not null,
dept_id         varchar(20)     not null,
manager_id      INT             null,
salary          INT             not null,
expertise       varchar(20)     not null
);


insert into employees(employee_id,first_name,last_name,dept_id,manager_id,salary,expertise)
select 100,'John','White','IT',103,120000,'Senior' union
select 101,'Mary','Danner','Account',109,80000,'junior' union
select 102,'Ann','Lynn','Sales',107,140000,'Semisenior' union
select 103,'Peter','O''connor','IT',110,130000,'Senior' union
select 106,'Sue','Sanchez','Sales',107,110000,'Junior' union
select 107,'Marta','Doe','Sales',110,180000,'Senior' union
select 109,'Ann','Danner','Account',110,90000,'Senior' union
select 110,'Simon','Yang','CEO',null,250000,'Senior' union
select 111,'Juan','Graue','Sales',102,37000,'Junior';

COMMIT

select * from employees;

select employee_id, 
first_name, 
last_name, 
salary,
rank() over (order by salary desc) as ranking
from employees
order by ranking

with employee_ranking as (
    select employee_id, 
    first_name, 
    last_name,
    salary,
    rank() over (order by salary desc) as ranking
    from employees
)
select employee_id,
first_name, 
last_name,
salary
from employee_ranking
where ranking <=5
order by ranking

with employee_ranking as (
    select employee_id, 
    first_name, 
    last_name,
    salary,
    rank() over (order by salary desc) as ranking
    from employees
)
select employee_id,
first_name, 
last_name,
salary, 
ranking
from employee_ranking
where ranking <=5
order by ranking

select employee_id, 
first_name, 
last_name, 
max(salary)
from employees
group by employee_id, first_name, last_name
having max(salary) < (
    select max(salary) from employees
)

with employee_ranking as (
    select employee_id, 
    first_name,
    last_name,
    dept_id,
    salary,
    rank() over (partition by dept_id order by salary desc) as ranking
    from employees
)

select employee_id, 
first_name, 
last_name, 
dept_id,
salary
from employee_ranking 
where ranking = 2

with employee_ranking as (
    select first_name, 
    last_name, 
    salary,
    ntile(2) over (order by salary desc) as ntile
    from employees
)
select first_name, 
last_name, 
salary
from employee_ranking
where ntile = 1
order by salary

with employee_ranking as (
    select first_name, 
    last_name, 
    salary, 
    ntile(4) over ( order by salary desc) as ntile
    from employees
)
select first_name, 
last_name, 
salary
from employee_ranking
where ntile = 4

select employee_id,
first_name, 
last_name, 
salary,
row_number() over ( order by employee_id ) as RowNo
from employees
order by RowNo asc


select e1.first_name + ' ' + e1.last_name as "Name",
e1.dept_id,
e2.first_name + ' ' + e2.last_name as "Manager"
from employees e1 inner join employees e2
on e1.manager_id = e2.employee_id

select first_name, 
last_name, 
salary 
from employees
where salary > ( select avg(salary) from employees)

select first_name, 
last_name,
salary
from employees e1
where salary > ( select avg(salary) from employees e2 where e2.dept_id = e1.dept_id)

select dept_id, 
expertise,
sum(salary) as 'Total Salary'
from employees
group by dept_id, expertise

select dept_id, 
expertise, 
sum(salary) as 'Total Salary'
from employees
group by rollup(dept_id, expertise)

select 
case when salary <= 75000 then 'low'
     when salary > 75000 and salary <= 100000 then 'medium'
     when salary > 100000 then 'high'
end as salary_category,
count(*) as 'No of employees'
from employees
group by 
case when salary <= 75000 then 'low'
     when salary > 75000 and salary <= 100000 then 'medium'
     when salary > 100000 then 'high'
end

drop table sales

create table sales (
    product_id      int         not null,
    sale_date       DATE        not null,
    amount          FLOAT       not null
)

insert into sales(product_id, sale_date, amount)
select 1, '2021-01-01', 100 union 
select 2, '2021-01-15', 200 union
select 1, '2021-02-01', 300 union
select 2, '2021-02-15', 400 union
select 1, '2022-01-10', 200 union
select 1, '2022-02-05', 100 union
select 2, '2022-01-27', 200 union
select 2, '2022-02-12', 400

select DATEPART(year, sale_date) as Year,
DATEPART(month, sale_date) as Month, 
sum(amount) as Total
from sales
group by DATEPART(year, sale_date), DATEPART(month, sale_date)


/* NOT WORKING  
select 
datepart(year, sale_date) as Year,
datepart(month, sale_date) as Month,
sum(amount) as 'Total'
from sales
group by Year, Month
*/


select * from employees



